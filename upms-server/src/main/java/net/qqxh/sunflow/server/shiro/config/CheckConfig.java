package net.qqxh.sunflow.server.shiro.config;

import com.chungkui.check.core.AbstractCheckConfigCacheService;
import com.chungkui.check.core.BaseChungkuiCheckConfiguration;
import com.chungkui.check.expression.CheckConfigCacheService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


/**
 * @author zhs
 */
@Configuration
public class CheckConfig extends BaseChungkuiCheckConfiguration {

    @Bean
    public CheckConfigCacheService configCacheService(){
       return new AbstractCheckConfigCacheService() {
            @Override
            public String remoteGet(String key, String defaultValue) {
                return null;
            }
        };
    }
}
